package com.order.ms.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.order.ms.domain.Order;

@Repository
public interface OrderDao extends JpaRepository<Order, String> {

}

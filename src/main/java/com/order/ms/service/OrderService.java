package com.order.ms.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.ms.dao.OrderDao;
import com.order.ms.domain.Order;

@Service
public class OrderService implements IOrderService {

	@Autowired
	private OrderDao orderRepository;
	
	@Override
	public List<Order> findAll() {
		return (List<Order>) orderRepository.findAll();
	}

	@Override
	public Order findById(String id) {
		return orderRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("This id: " + id + ( " not available")));
	}

	@Override
	public void deleteById(String id) {
		orderRepository.deleteById(id);
		
	}

	@Override
	public Order save(Order order) {
		return orderRepository.save(order);
	}

}

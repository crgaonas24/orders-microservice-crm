package com.order.ms.service;

import java.util.List;

import com.order.ms.domain.Order;

public interface IOrderService {
	
	public List<Order> findAll();
	public Order findById(String id);
	public void deleteById(String id);
	public Order save(Order order);

}

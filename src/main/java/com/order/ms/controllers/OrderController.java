package com.order.ms.controllers;

import java.util.List;

import org.hibernate.annotations.SourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.order.ms.domain.Order;
import com.order.ms.service.OrderService;

@RestController
@RequestMapping(path = "/api/v1//orders")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class OrderController {

	@Autowired
	private OrderService service;
	
	// List all orders
	@GetMapping
	public List<Order> getOrders(){
		return service.findAll();
	}
	
	// Save new order
	@PostMapping
	public ResponseEntity<Order> saveOrder(@RequestBody Order order) throws JsonProcessingException{
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(order));
		
    }

	// Get order by Id
	@GetMapping("/{id}")
	public ResponseEntity<Order> getOrderById(@PathVariable String id){
		Order order = service.findById(id);
		return ResponseEntity.status(HttpStatus.OK).body(order);
	}

	// Delete order by Id
	@DeleteMapping("/{id}")
	public void deleteOrderById (@PathVariable String id){

		try {
			Order order = service.findById(id);
			if (order != null) {
				service.deleteById(id);
			}
			
		} catch (Exception e) {
			
		}
		
		
	}
	
	
	
}
